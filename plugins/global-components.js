import Vue from 'vue'

import LoadingIcon from '~/components/common/loading-icon'
Vue.component('LoadingIcon', LoadingIcon)

import CardPuppyInfo from '~/components/card-puppy-info'
Vue.component('CardPuppyInfo', CardPuppyInfo)
